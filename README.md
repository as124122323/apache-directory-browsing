# Apache目錄瀏覽

## 功能簡介：
- 雖然大部分情況下，都會將Apache目錄瀏覽關閉以策安全，但某些特殊需求還是會需要開啟。
- 套用apaxy美化目錄瀏覽

## 佈署方式（develop）：
1. 取得專案：`git clone https://gitlab.com/as124122323/apache-directory-browsing.git`
2. 進入專案目錄：`cd apache-directory-browsing`
3. 建立Image：`bash docker/build_image.sh`
4. 啟動Docker服務：
	- 啟動指令：`bash docker.develop/deploy_stack.develop.sh`
	- 啟動服務包含：
		- apache：apache服務
	- 若要關閉Docker服務則運行：`bash docker.develop/remove_stack.develop.sh`

## 測試網址（develop）：
- domain：你的機器IP 或 127.0.0.1
- port：8001
- 目錄瀏覽URL：`http://{domain}:8001/browsing/`


## apaxy美化目錄使用筆記：
1. 下載`apaxy`
    - 網址：`https://github.com/oupala/apaxy/tags`
    - 找最新的一版載即可（本專案是用2.4.0）
2. 將載下來的壓縮檔解壓縮，並找到`apaxy`目錄（我們只需要這個目錄底下的東西）
    - 示意圖：
        ![apaxy_content](https://gitlab.com/as124122323/apache-directory-browsing/-/raw/main/readme_img/apaxy_content.png?inline=false)
3. 修改`htaccess.txt`，並重命名為`.htaccess`
    - 將文內的 `{FOLDERNAME}` 全都替換成路由(本範例是替換成`/browsing`)，之後會在`apaxy.conf`裡設定路由
    - `htaccess.txt`中的內容：
        ```sh
        # replace {FOLDERNAME} with your directory url
        #
        # if your directory is http://mywebsite.com/share/ you would change to:
        #
        # AddIcon /share/theme/icons/empty.svg ^^BLANKICON^^
        ```
4. 將其中的`theme`資料夾和`.htaccess`，放到欲瀏覽的目錄中（本範例是放在`Docker`容器中的`/var/www`）
    - 在`build image`時，透過設定`Dockerfile`複製進去的
        ```sh
        COPY ["docker/build_config/apache/apaxy/.htaccess", "/var/www/.htaccess"]
        COPY ["docker/build_config/apache/apaxy/theme", "/var/www/theme"]
        ```
    - 容器中示意圖：
        ![apaxy_in_container](https://gitlab.com/as124122323/apache-directory-browsing/-/raw/main/readme_img/apaxy_in_container.png?inline=false)
5. 編寫`Apache conf`檔（本範例中叫`apaxy.conf`）
    - 在`build image`時，透過設定`Dockerfile`複製進去的
        ```sh
        COPY ["docker/build_config/apache/apaxy.conf", "/etc/apache2/sites-enabled/apaxy.conf"]
        ```
    - 設定檔內容：
        - 本範例是瀏覽`/var/www`目錄，路由設為`/browsing`（`Alias /browsing/ /var/www/`）
        ```sh
        Listen 8001
        <VirtualHost *:8001>
            ErrorLog "/proc/self/fd/1"
            CustomLog "/proc/self/fd/2" combined
            
            # 設定路由並開通訪問權限，最後面要以/結尾才可讀得到
            Alias /browsing/ /var/www/
            <Directory /var/www/>
                Options Indexes FollowSymLinks
                Require all granted
                AllowOverride All
                Order deny,allow
                Allow from all
                IndexOptions NameWidth=128
                IndexOptions FancyIndexing
                IndexOptions DescriptionWidth=256
                IndexOptions ScanHTMLTitles
                IndexOptions FoldersFirst
                IndexOptions VersionSort
                IndexOptions Charset=utf-8
                IndexOptions HTMLTable
                ServerSignature off
            </Directory>
        </VirtualHost>
        ```
6. 允許`Apache`瀏覽欲瀏覽目錄的權限（本範例是放在`Docker`容器中的`/var/www`）
    - 在`build image`時，在`Dockerfile`中設定
        ```sh
        # www-data為apache用戶及群組
        RUN chown -R www-data:www-data /var/www \
            && chmod -R 755 /var/www
        ```
